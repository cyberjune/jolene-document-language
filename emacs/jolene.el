;;; jolene.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 June Kelly
;;
;; Author: June Kelly <https://halt.wtf>
;; Maintainer: June Kelly <june@cnjctr.com>
;; Created: November 22, 2021
;; Modified: November 22, 2021
;; Version: 0.0.1
;; Keywords: jolene document markup
;; Homepage: https://github.com/junebug/jolene
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'rx)

(defvar jolene-constants
  '("!@" "!define"))

(defvar jolene-keywords
  '("!code" "!text" "!todo" "!start" "!done"))

;; I'd probably put in a default that you want, as opposed to nil
(defvar jolene-tab-width 2 "Width of a tab for MYDSL mode.")

;; Two small edits.
;; First is to put an extra set of parens () around the list
;; which is the format that font-lock-defaults wants
;; Second, you used ' (quote) at the outermost level where you wanted ` (backquote)
;; you were very close
(defvar jolene-font-lock-defaults
  `((
     ;; stuff between double quotes
     ("\"\\.\\*\\?" . font-lock-string-face)
     ;; ; : , ; { } =>  @ $ = are all special elements
     (":\\|,\\|;\\|{\\|}\\|=>\\|@\\|$\\|=" . font-lock-keyword-face)
     (,(rx "{"
          (zero-or-more anything)
          (group "!verbatim")
          (zero-or-more anything)
          "}" ) . (1 font-lock-function-name-face)) ;; TODO: work out how to generalize this
     ( ,(regexp-opt jolene-keywords 'non-nil) . font-lock-function-name-face)
     ( ,(regexp-opt jolene-constants 'non-nil) . font-lock-constant-face))))

(define-derived-mode jolene-mode outline-mode "Jolene"
  "JOLENE mode is a major mode for editing Jolene files"
  ;; you again used quote when you had '((jolene-hilite))
  ;; I just updated the variable to have the proper nesting (as noted above)
  ;; and use the value directly here
  (setq font-lock-defaults jolene-font-lock-defaults)

  ;; when there's an override, use it
  ;; otherwise it gets the default value
  (when jolene-tab-width
    (setq tab-width jolene-tab-width))

  ;; for comments
  ;; overriding these vars gets you what (I think) you want
  ;; they're made buffer local when you set them
  (setq comment-start ";")
  (setq comment-end "")

  (modify-syntax-entry ?\; "< b" jolene-mode-syntax-table)
  (modify-syntax-entry ?\n "> b" jolene-mode-syntax-table)

  (setq outline-regexp "[\#\f]+")

  )

(provide 'jolene-mode)
;;; jolene.el ends here
