|title: "Jolene Example"
 author: "June"
 date: "2021-11-20"|

# Introduction

This document describes, in high-level terms, the Jolene document language.

Jolene is a document language inspired by org-mode and markdown, and is
intended to serve as a format for authoring documents in plain text, that
will be read as-is, and rendered to HTML, PDF, or other formats.

# Basic structure

## Segments

A Jolene document is made up of segments, separated by blank lines. A
text paragraph (like this one) is a simple kind of segment.

A segment can be delimited explicitly by wrapping in either square brackets or curly braces, like so:

[
This is a segment with square brackets.
]

{
  This is a segment with curly braces.
}

Text content within square brackets will be processed by jolene, while text
content within curly braces, will treated "verbatim", and not processed by jolene.

There are two kinds of segment, block-segment and inline-segment, referred to as
"blocks" and "inlines".

Text blocks can contain inline-segments, delimited with square brackets or curly
braces, like so: [this is an inline segment]. When an inline-segment contains white-space at the
beginning or end, this white-space is stripped from the rendered output. For
example, [   in this segment, the extra spaces will be stripped out        ].

A segment with square brackets or curly braces is an "explicit" segment, in contrast with the
"implicit" segments defined by surrounding blank lines. Inline-segments must always
be explicit. Explicit segments can be used to group lines together in one segment,
including blank lines that would ordinarily separate them into multiple segments.

## Escaping characters

Any special characters used by Jolene can be escaped with a backslash (`\`).
Example: These are just plain square brackets: \[, and \ ].

Escaping can also be achieved by wrapping the content in a verbatim segment. For
another example, {these are just plain square brackets: [  ], aren't they pretty?}

## Comments

Any line beginning with two semi-colons (`;;`) is a comment, and will be ignored by
the jolene processor, and will not appear in the rendered output.

## Headings

Jolene is an outliner, like org-mode. This means a Jolene document is organized
as a tree, where headings define the tree structure.

Headings begin with one or more hash (`#`) characters, followed by a space, then
followed by some text. The number of `#`s at the start of the heading determines
its "level", with deeper levels nesting inside shallower levels.

Text editors should provide shortcuts to manipulate the document outline. For
example, it should be possible to shift outline levels up or down, or to add a
new heading at the current level.

# Formatting

Text can be formatted as such: *bold*, /italic/, _underline_, `code`,
and ~strikethrough~.

# Properties |id: "properties"|

Properties can be attached to any segment, by listing the properties between
pipe characters, like so: `|foo, bar, baz: quux|`. This construct is called a "property
list", or a `plist` for short.

Within a property list, comma characters count as whitespace, and are thus
optional.

The elements in a property list are either "atoms", or key-value "pairs".  An
atom is simply a name, such as `foo`. A pair is a name, followed by a colon,
then followed by a value, like `foo: bar`.

Values are text strings, either with or without surrounding quote characters
(`'`, or `"`). A value that contains whitespace must be surrounded by quotes.

The elements in a property list are /ordered/, meaning the property list should
be treated as a list of properties, not like an unordered map.

Property lists must be placed at the beginning or end of a segment. The
following examples are all equivalent:

{|~code: jolene|
  # Foo |bar|

  # |bar| Foo

  # Foo         |bar|

  # Foo
  |bar|

  |bar|
  # Foo

  [|~heading, level: 1, bar| Foo]
}

In each case, the result is a Heading at level one, with title "Foo", and one
property "bar".

(Note that the `#` character is considered part of the "preamble" of a heading
segment, so placing the property list after the `#` still counts as the start of
the segment. This will be clarified in a later draft.)

If multiple property lists are defined for the same segment, they are merged
together, in order.

## Escaping

Property lists can be "escaped", and treated verbatim, by prefxing them with a
backslash character (`\`).

Example

{|~code: jolene|
  \|foo: bar| This block begins with a pipe character, for some reason.
}

Pipe characters occuring within the text content of a segment (not at the beginning
or end) will not be interpreted as a property list, and thus does not need to be
escaped.

## IDs |id: ids-section|

One particularly useful property is `id:`, which can be used to assign IDs to
segments.

# Environments |id: environments|

Every segment sets an "environment" for its content to be interpreted within.
The default environment is `text`, in which content is treated as plain text,
except for jolene formatting. Other environments might change the meaning
of the content, in relation to the rendered document output.

The environment of a segment can be set explicitely, by setting the appropriate
property on the segment. Property atoms that name environments begin with a tilde
character (`~`).

## Types of environment

### Text styling

The various text formatting options, such as *bold* and /italic/, are really
just environments.

Bold text can be formed like so: [|~bold| this text is bold].

The same can be done with other text format environments:

- `~bold`
- `~italic`
- `~underline`
- `~strikethrough`

Example:

{|~code: jolene|
  Here is some [|~bold| nice bold text].

  Here is some [|~italic| text in italics].
}

### Code

Code environments are defined with the `~code` property. Optionally, the language
can be specified by attaching a value to the `~code:` property.

Example:

{|~code: javascript|
  const foo = bar()
}

{|~code|
  var text = "this is code"
}

The `~src`, and `~source` properties can be used as aliases for `code`.

The code environment should be used in a verbatim segment (with curly braces
(`{}`)), to prevent jolene from processing the code content. Tooling should
issue a warning if `~code` is used on a normal text segment.

Code will be rendered with a monospace font, and syntax highlighting for the
specified language, if available.

### Quotes

Quote environments are defined with the `~quote` property.

Example:

[|~quote|
Some are born great, some achieve greatness, and some have greatness
thrust upon them.

-- William Shakespeare
]

### Links

Links are defined with the `~link` property.

Example:

[|~link href: "https://example.com"| an example website]

The following are also valid:

{|~code: jolene|

  [an example website |~link "https://example.com"|]

  [an example website |~link, href: "https://example.com"|]
}

As a short-hand, when the `~link` property atom is followed immediately by a
string containing a URL, this string is taken as the URL for the link, making
the `href:` property optional.

The `~hyperlink` property can be used as an alias for `~link`.

#### Short-form links

If the content of a link segment is a URL, then just the `~link` property will
suffice to turn the segment into a link.

Example:

[|~link| https://example.com]

#### Linking to IDs

A link can point to an ID within a Jolene document, by naming the ID, prefixed
with a `^` character.

For example, this is a link to the [IDs section |~link ^ids-section|] of this
document.


### Images

Images are defined with the `~image` property.

Example:

[a picture of a cat |~image "cat.jpeg"|]

The text within the environment becomes the alt-text for the image.

Similar to the `link` environment, images may be embedded within text segments.


## Explicit and implicit environment segments

By convention, enviornment segments should be defined explicitely, with square
brackets, as in the examples above. However, when the environment content does
not contain blank lines, it is possible to omit the square brackets, and leave
the segment implicit.

Examples:

|~code: python|
some_number = 1

|~verbatim|
This text is verbatim.

|~quote|
This is a quote.

However, this style is not encouraged, and should be limited to rare cases.
Authors should prefer to define environment segments explicitely, for the sake of
readability.

## Everything is an environment segment with properties

Ultimately, every element in a jolene document is a segment with and environment
and other properties attached. The more "natural" way to write a jolene document
is to not spell out each enviornment explicitely, but to rely instead on what
amounts to "syntax sugar" for common environment types.

Headings with a `#` prefix are a syntax sugar for a `~heading` environment with
a `level: n` property. Code between backticks is just sugar for a `~code`
environment, and plain paragraphs of text are sugar for the `~text` environment.

For example, the following documents are equivalent:

{|~code: jolene|

  |title: "Segment Example"|

  # Some heading

  A paragraph of text.

  {|~code: javascript|
    const x = 1
  }
}

{|~code: jolene|

  [|~document, title: "Segment Example"|
   [|~heading, level: 1| Some heading]
   [|~text| A paragraph of text.]
   {|~code: javascript|
     const x = 1
   }
   ]
}

TODO: Clarify syntax sugar types, preambles, and so on

## Notes on segment formatting

There are several ways to format segments and environments, here are a few examples:

{|~code: jolene|

  |~quote| A simple quotation

  [|~quote|
  A longer quotation
  with brackets on separate lines
  ]

  [|~quote| A longer
  quotation spanning across
  multiple lines, with brackets
  on the same lines as the text]
}

The whitespace at the beginning and end of the square brackets is not relevant.

## Extensions

Various other kinds of environment could be implemented as extensions to the
jolene tooling.

# Lists

Lists are formed by prefixing lines with the dash (or minus) character (`-`).

Example:

- Cat
- Dog
- Squirrel
- Walrus

Lists may be nested within other lists:

- A
  - foo
- B
  - bar
  - baz
- C

By default, lists are "un-ordered", in that the elements are not numbered.

A numbered, or "ordered" list can be created by attaching the `ordered-list`
property:

|~ordered-list|
- First
- Second
- Third

The default kind of unordered list can be specified explicitly with the
`~unordered-list` property.

# Horizontal rule

To make a horizontal rule, use three or more dashes on a line:

{|~code: jolene|
  ---
}

# Task management

Jolene can be used to keep track of tasks, and their status, by attaching
certain properties to headings. Editor tooling should provide convenient
shortcuts for manipulationg these properties.

The relevant properties are as follows:

- `!project`
- `!idea`
- `!todo`
- `!start`
- `!wait`
- `!hold`
- `!done`
- `!trash`
- `!yes`
- `!no`

## Priorities

Priorities can be assigned to headings by adding a value to the task atom. Like
so: `!todo: A`

Valid values are:

- `.`: Emergency
- `A`: Top priority
- `B`: Lesser priority
- `C`: Less again
- `D`: Lowest priority

Example:

{|~code: jolene|
  ### |!todo: A| Finish this document
}

## Task overview

Editor tooling should provide an interface to view tasks and priorities, and to
manipulate the same.

# Variables

Variables can be defined like so:

|@define foo: bar|

And used like so:

{|~code: jolene|
  This is some text with a variable: [|@ foo|]
}

Variables defined at the top level of the document are available throughout the
document.

A full example:

{|~code: jolene|
  |@define name: "June", age: 32|

  ## Cool things to know about this person

  [|@ name|] is [|@ age|] years old.
}

## Variables as property values

Inside a property list, a variable can be used as the value of a pair, by
prefixing its name with an `@` character.

Example:

{|~code: jolene|
  |@define website: "https://example.com"|

  Here is a link [to a cool site |~link href: @website|].

  Here is the same link: [site |~link @website|].
}

This variable substitution only happens when the value is not surrounded by quotes.

# Tables

Tables can be defined like so:

| Name  | Likes    | Age |
|-------+----------+-----|
| Alice |  Cheese  |  17 |
| Bob   |  Kittens |  25 |

Editor tooling should provide shortcuts for creating and manipulating tables.
Tooling should also provide functionality to treat tables as a spreadsheet

# References

TODO

# Footnotes

TODO

# Definition Lists

TODO

# Export and publishing

Jolene is designed to be converted to html, latex, and PDF.
