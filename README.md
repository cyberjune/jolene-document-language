# Jolene, an organized document language

Jolene is a plain-text document language inspired by org-mode and markdown,
designed with tooling and cross-editor compatibility in mind. This project
intends to define the Jolene language, and provide a reference implementation of
a parser.

Jolene is intended as a replacement for Org-mode, combined with some of the niceties from
Markdown, and with cross-platform editor support for its smart features. With a
smart editor a Jolene file becomes a lively document, working _with_ the author
to get things done.

![A cartoon drawing of a woman's hair](head.png)

## Motivation

Org-mode is a wonderful productivity system, but it is tied to a single editor
(emacs), and suffers from rather lumpy and unusual syntax. On the other hand,
Markdown is a nice clean format, but suffers from inconsistencies between
implementations, ambiguities in parsing, and a lack of the smart features that
make Org-mode great.

Jolene aims to solve these problems by:

- Specifying the language in full
- Having a simple document model
- Keeping the syntax small and consistent
- Offering both syntax sugar and explicit constructs
  - (and defining the syntax sugar _in terms of_ the explicit syntax)
- Defining the smart operations that editor plugins are expected to perform
- Placing all the "smarts" in a shared library, or language server
- Defining an extension interface, so the language can evolve and adapt to the
  needs of real authors in the wild

## Language description

A Jolene document is composed of segments containing text. The top level of the
document is a  list of "block" segments, and each block segment can contain
"inline" segments.  Block segments are separated by blank lines. Optionally, the
block can be wrapped in square brackets (`[ ]`). Inline segments must always be
wrapped in square brackets.

There is a special kind of block segment, called a "verbatim block", which is 
denoted by curly braces (`{ }`), and indenting the content of the block by two 
spaces. Verbatim blocks are not processed further by jolene, their content is 
taken literally.

Each segment can have a property list between pipe characters (`| |`), and the
property list can define an "environment" in which the segment content will be
interpreted, such as `~text`, `~code`, or `~quote`. Jolene also defines syntax
sugar for commonly used environments, like `~heading` and `~list`.

This is a simple Jolene document:

```
|title: "Jolene Example"
 author: "Alice"
 date: "2021-11-01"|

# Welcome to Jolene     |id: welcome|

This is an example document, written in the Jolene language. 
I'm just going to ramble for a bit to fill space.
Check out our list of [cool animals |~link ^cool-animals|] 
later in this document. 

[
  This is an explicit block segment, unlike the previous
  block, which was left implicit.
]

Here's some code:

{|~code: javascript|
  const projectName = "Jolene"
}

And here's a simple list of cool animals:

|id: cool-animals|
- Cat
- Stoat
- Pigeon
- Walrus
```

The following document is equivalent to the previous document:

```
|title: "Jolene Example",
 author: "Alice",
 date: "2021-11-01"|

[|~heading level: 1, id: welcome| Welcome to Jolene]

[|~text|
This is an example document, written in the Jolene language. 
I'm just going to ramble for a bit to fill space.
Check out our list of [cool animals |~link ^cool-animals|] 
later in this document.]

[
  This is an explicit block segment, unlike the previous
  block, which was left implicit.
]

[|~text|
Here's some code:]

{|~code: javascript|
  const projectName = "Jolene"
}

[|~text|
And here's a simple list of cool animals:]

[|~unordered-list, id: cool-animals|
 [Cat]
 [Stoat]
 [Pigeon]
 [Walrus]]
```

Jolene documents are typically saved with a `.jd` extension.

See [jolene.jd](jolene.jd) for a more detailed description of the language.

## Features and examples

### Outline Headings

Jolene is an outliner, much like Org-mode. However, Jolene uses atx-style
headings, similar to markdown.

```
# Some heading

## A Sub-heading
```

#### Task status

Like in Org-mode, headings can be used as a task list (or tree).

```
# My Tasks

## |!todo| Buy a cake

## |!todo| Eat the cake

It was delicious!

## |!start| Make a shopping list

- Eggs
- Bread
- Cheese

## |!done| Pay the gas bill

£139 for the month.
```


#### Priorities

Tasks can be assigned priorities, from `A` to `D` (and `!`, for emergencies).

```
# My task list

## |!start: A| Do this first

## |!todo: C| Do this later
```

### Property lists

Segment can have properties attached, in a list at either the beginning or end of
the segment:

```
## Some heading
|id: some-nice-heading|
```

As you can see, properties are used to specify the "environment" of the segment.
Environment names start with a `~` character:

```
[|~quote, id: example-environment| 

This is a quote by a famous person.

-- John Wayne
]
```

### Text formatting

```
Text can be formatted like so: *bold*, /italic/, _underline_, 
`code`, and ~strikethrough~.

Or, we can be more explicit: [|~bold| this text is bold], and
[|~italic| this text is italic].

{|~code: python|
  remark = "This is a code block"
}

[|~quote|
This is a quote
]
```

Note how we use a verbatim segment (`{  }`) for the code block, 
so the contents are not interpreted by jolene.

### Links

```
# Link example
|id: link-example-section|

This is a link to a [cool website |~link https://example~com|].

We can also link to IDs within the document, [here |~link ^link-example-section|].
```

### Images

```
Here's a nice picture:

[photograph of a cat |~image "cat~jpeg"|]
```

### Lists

```
This is an unordered list:

- Cat
- Dog
- Frog

Which is the same as:

|~unordered-list|
- Cat
- Dog
- Frog

Which is also the same as:

[|~unordered-list|
 [Cat]
 [Dog]
 [Frog]]

This is an ordered-list:

|~ordered-list|
- First
- Second
- Third
```

### Variables

```
|@define, name: "Alice"|

We should talk to [|@ name|] about this.
```

### Escaping

Special characters can be escaped by prefixing them with a backslash (`\`).
Alternatively, whole runs of text can be escaped by wrapping them in a verbatim
segment, with curly braces ('{ }'), like so:

```
This is just some {plain text, with [ square brackets ] and so on}.
```

## Status

This project is very young. Don't expect anything usable yet.


## License

This project is currently unlicensed. This situation will change in future.
